********************************************************************
                        BACKGROUND VIDEO MODULE
********************************************************************

INTRODUCTION
--------------------------------------------------------------------
The Background Video allows you to add videos as background block
for your website.

REQUIREMENTS
--------------------------------------------------------------------
 * This module does not require installation of additional modules.

RECOMMENDED MODULES
--------------------------------------------------------------------
 * There are no recommended modules.

INSTALLATION
--------------------------------------------------------------------
 * Install as you would normally install a contributed Drupal module.
   See:

CONFIGURATION
--------------------------------------------------------------------
 * Go to /admin/config/development/bg_video to configure the module
   after installation.

 * Go to admin/structure/block to configure the Block after installation.

Demo
--------------------------------------------------------------------
 * http://www.athensmexyachting.com/

MAINTAINERS
--------------------------------------------------------------------
 Current maintainer:
 * Mohit Parashar
