<?php
/**
 * @file
 * Background video block template file.
 */
?> 
<div class="bg-video-module">
  <div class="video-container">
    <div class="title-container">
      <?php if (isset($bg_video['bg_video_title'])): ?>
      <div class="headline">
        <h1><?php print $bg_video['bg_video_title']; ?></h1>
      </div>
      <?php endif; ?>
      <?php if (isset($bg_video['bg_video_short_text'])): ?>
      <div class="description">
        <div class="inner">
            <?php print $bg_video['bg_video_short_text']; ?>
        </div>
      </div>
      <?php endif; ?>
    </div>
    <video <?php echo drupal_attributes($bg_video['bg_video_attributes']); ?> class="fillWidth">
      <source src="<?php print $bg_video['bg_video_src_url']; ?>"/>
      Your browser does not support the video tag. I suggest you upgrade your browser.
    </video>
    <?php if (isset($bg_video['bg_video_poster_url'])): ?>
    <div class="poster hidden">
      <img src="<?php print $bg_video['bg_video_poster_url']; ?>" alt=""  />
    </div>
    <?php endif; ?>
  </div>
</div>
